+++
title = "How to Study and Learn"
+++
<!--: .wrap .size-100 bg=aligncenter bgimage=images/brain.jpg -->


## **How to Study and Learn**

<!--: .text-intro -->Evidence-based tips.



---

<!--: bg=bg-apple bg=aligncenter .wrap bgimage=images/ai.png bgpos=left-bottom -->

## **Two Modes of Thinking**

<!--: .text-intro -->Focused and Diffuse

<!-- : .wrap -->
{{< div class="content-right" >}}

<ul>
Focused Thinking:
<li>Deliberately practicing or thinking about something, giving it all your attention.</li>
<li>Allows us to break the task up into smaller pieces.</li>
</ul>

<br>

<ul>
Diffuse Thinking:
<li>Happens when your mind relaxes, providing space for daydreaming and wandering thoughts.</li>
<li>Allows your brain to make bigger-picture connections involving your new skills so they become second nature.</li>
</ul>

<br>

{{< svg fa-comment-o >}} Each of these modes helps us learn in different ways but the brain can think in only one of the modes at a time.

{{< /div >}}


---

<!--: bg=bg-apple bg=aligncenter .wrap bgimage=images/clock.png bgpos=right-bottom -->

## **Pomodoro Technique**

<!-- : .wrap -->
{{< div class="content-left" >}}
<ol>
	<li>Decide on the task to be done.</li>
    <li>Set the pomodoro timer (traditionally to 25 minutes).</li>
    <li>Work on the task.</li>
    <li>End work when the timer rings and put a checkmark on a piece of paper.</li>
    <li>If you have fewer than four checkmarks, take a short break (3–5 minutes) and then return to step 2; otherwise continue to step 6.</li>
    <li>After four pomodoros, take a longer break (15–30 minutes), reset your checkmark count to zero, then go to step 1.</li>
</ol>

<br>

{{< svg fa-comment-o >}} The Pomodoro technique is an effective way to tackle procrastination

{{< /div >}}

---

<!--: bg=bg-apple bg=aligncenter .wrap bgimage=images/root.png bgpos=left-bottom -->

## **Mindset**

<!--: .text-intro -->Fixed and Growth Mindset

<!-- : .wrap -->
{{< div class="content-right" >}}
<ul>
Fixed Mindset:
<li>People with a "fixed mindset" believe that abilities are mostly innate and interpret failure as the lack of necessary basic abilities.</li>
<li>In a fixed mindset, individuals believe their basic abilities, their intelligence, their talents, are just fixed traits.</li>
<li>Those with a fixed mindset believe there is no increasing their talents or abilities.</li>
<li>They believed they will be born with all the ability they will have.</li>
<li>Those with a fixed mindset only enjoy hearing about their success.</li>
<li>On they other side, those with a fixed mindset dread failure because they see it as never being able to succeed in the future.</li>
</ul>
<br>
<ul>
Growth Mindset:
<li>Individuals with a "growth" mindset are more likely to continue working hard despite setbacks.</li>
<li>Those with a "growth mindset" believe that they can acquire any given ability provided they invest effort or study.</li>
<li>In a growth mindset, individuals understand that their talents and abilities can be developed through effort, good teaching, and persistence.</li>
<li>They do not necessarily think everyone is the same or anyone can be Einstein, but they believe everyone can get smarter if they work at it.</li>
</ul>
<br>

<q>
	“Whether you think you can, or you think you can't--you're right.” - Henry Ford
</q>

{{< /div >}}

---

<!--: bg=bg-apple bg=aligncenter .wrap bgimage=images/thank-you.png bgpos=right-bottom -->

<!--: .wrap -->

# **Credits**:

### Coursera
[{{< svg fa-link >}}Learning How To Learn](https://www.coursera.org/learn/learning-how-to-learn)


### Mindset
[{{< svg fa-link >}}Mindset](https://news.stanford.edu/pr/2007/pr-dweck-020707.html)


### Images:

[{{< svg fa-link >}}Brain](https://pixabay.com/illustrations/brain-electrical-knowledge-migraine-1845962/)

[{{< svg fa-link >}}Clock](https://pixabay.com/vectors/clock-analog-face-blue-time-timer-32380/)

[{{< svg fa-link >}}AI](https://pixabay.com/illustrations/artificial-intelligence-brain-think-3685928/)

[{{< svg fa-link >}}Thank You](https://pixabay.com/illustrations/thank-you-letters-2204270/)

[{{< svg fa-link >}}Root](https://pixabay.com/illustrations/root-tree-logo-clip-art-tree-roots-1151254/)


### Technology:

[{{< svg fa-link >}}Webslides](https://webslides.tv)

[{{< svg fa-link >}}Hugo Theme](https://github.com/RCJacH/hugo-webslides)
